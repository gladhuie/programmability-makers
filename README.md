# JokeBot μService

A simple backend Bot that can send a joke to a person on Webex Teams. 

## Step by Step lab 

_Before you start make sure to have Python3, Postman, Git and WebexTeams running_

_Docker is nice to have_

## The repo structure

### Task 1. Familiarise yourself with the folder structure.
```
├── Dockerfile          <-- This is the definition of your Docker container.
├── README.md           <-- This is the file you are reading :).
├── jokeBot             <-- This is the Bot python package.
│   ├── __init__.py     <-- This file helps python understand that the jokeBot folder is a Python package.
│   ├── app.py          <-- This is the API server that will run your Code.
│   ├── jokeApi.py      <-- [Update Me] You will add your code to retrieve a joke here.
│   └── webexApi.py     <-- [Update Me] You will add your code to send a webex message here.
└── requirements.txt    <-- This file lists all python packages required for your app to run.
```

---

## jokeApi.py Code

### Task 2. Update the get_a_joke function 

* Open `jokeApi.py` python file. The function `get_a_joke()` is missing actual code. Read the function docstring and continue.
```python 
def get_a_joke() -> str:
    ...
    return ""
```

* From the Postman request your made earlier retrieve the auto-generated python code and copy and paste it into the function.
Python interpreter understands that the code you paste belongs to the function if it's indexed under it like so :
```python
def get_a_joke() -> str:
    import  requests

    url = "https://icanhazdadjoke.com/"

    payload = {}
    headers = {
        'Accept': 'application/json',
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    print(response.text)
    return ""
```

Note : When using any modern IDE you can indent a block of code by selecting it then press *tab* or un-indent with *shift + tab* :
![indent](./img/indent.gif)

### Task 3. Retrieve only the joke from the response

* Let's trim a bit the code. First let's remove the "import requests" from the function. Requests is already imported. 
Although it works in python, you should never import a library in a function. Then let's return a joke and not an empty string.
The response you get from the external joke API looks like this :
```json
{
  "id": "R7UfaahVfFd",
  "joke": "My dog used to chase people on a bike a lot. It got so bad I had to take his bike away.",
  "status": 200
}
```

* Hilarious ... hum ... well the data structure format is called a JSON. Luckily for us python can easily translate json to dictionary object.
We don't want to return the all structure though but just the joke itself. You can easily do that by filtering the response json to the joke entry like that :
```python
...
response = requests.request("GET", url, headers=headers, data=payload)
return response.json()["joke"]
```

* The final file should look like this :
```python
import requests

def get_a_joke() -> str:
    """ You must implement that function.

    The function doesn't take argument

    The function must return a String, well.. the joke you fetched :)
    """
    url = "https://icanhazdadjoke.com/"

    payload={}
    headers = {
    'Accept': 'application/json'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    return response.json()['joke']

if __name__ == "__main__":
    """ 
    This part of the code will only be executed if you run directly jokeAPI.py file.
    It will not be visible to our app.py.

    It's often used to test individually a module.
    """
    print(get_a_joke())
```

### Task 4. Install libraries

The current python file is using a non-built-in library called `requests` : 
```python
import requests
```

If you were to run the code without the library installed you would get the following error :
```
Traceback (most recent call last):
  File "/Users/gladhuie/PycharmProjects/programability-makers/jokeBot/jokeApi.py", line 1, in <module>
    import requests
ModuleNotFoundError: No module named 'requests'
```

Luckily enough Python has a library manager called `pip`. We defined the list of library requirements for the project in the `requirements.txt` file. 

Content of `requirements.txt`:
```text
requests
uvicorn
fastapi
```

From vscode terminal run the following command:

Unix
```
cd programmability-makers
pip3 install -r requirements.txt
```
    
Windows
```
cd .\programmability-makers\
pip install -r requirements.txt
```

The packet manager will resolve libraries dependencies automatically

![](./img/4.png)

### Task 5. Try it :)

It's time to test the code ! you can run it from the UI directly or from the Terminal prompt in vscode.

#### From the vScode UI [prefered]

* save the current file by pressing `ctrl + s`
* click on the play button on the right hand side 
* select `Play Python File`

![](./img/1.png)

#### From the terminal 

Alternatively you can run the code from the terminal 

Unix
```
cd programmability-makers
python3 jokeBot/jokeApi.py
```
    
Windows
```
cd .\programmability-makers\
python jokeBot/jokeApi.py
```

Now let's try again :
```
➜  programability-day jokeBot/jokeApi.py
My boss told me to have a good day... so I went home.
```

## Python debugger

All went well ! Congratulations, but it's ~~sometimes~~ always useful to check step-by-step how the variables are being populated/changed/modified when the code is executed.

Python is an interpreted language, the interpreter executes/evaluates line-by-line your code.

You can specify `breakpoints` in your code by clicking aside the line number a red dot will appear.

![](./img/5.png)

It tells python where to momentarily stop the code execution in debug mode.

### Task 6. Position breakpoints and run the code

* position a `breakpoint` on line 17 and 19 
* try to run the file but this time select `Debug Python file`
* The code will stop at each `breakpoint` on the left-hand size panel you will see the content of all the variables at this point.
![](./img/2.png)
* to continue to the next `breakpoint` click the blue play button on top of your file or press `F5`
* Notice that the variables were updated and the variable `response` is now assigned.
![](./img/3.png)
* finish code execution by clicking the blue play button on top of your file or press `F5`

---

> :warning: *If you don't have Webex Teams skip this section*

## webexApi.py code [optional]

### Task 7. Webex API

* Great we have a joke function, now let's implement the message to webex, open webexApi.py
The function send_message_webex() is missing actual code. Read the function docstring and continue.
```python 
def send_message_webex(person : str,text : str) -> str:
    ...
    return ""
```

* From the Postman request your made earlier retrieve the auto-generated python code and copy paste it into the function.
Python understand that the code you paste belongs to the function if it's indexed under it like so :
```python
def send_message_webex(person : str,text : str) -> str:
    import requests

    url = "https://webexapis.com/v1/messages"

    payload = "{\n    \"toPersonEmail\" : \"gladhuie@cisco.com\",\n    \"text\" : \"Hello, it worked but this is not a joke yet !\"\n}"
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer MGRiYjIyOWUtOTFmZC00NDFkLTg2NWMtMDU0Y2JjNTBkZDYwYjdhYjdlNmEtNTNh_PF84_1eb65fdf-9643-417f-9974-ad72cae0e10f'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)
    return ""
```

### Task 8. Refactor code

* Let's refactor a bit the code now. Let's first remove the "import requests" sentence from the function. Then let's return the response we got from Webex :
```python
...
    response = requests.request("POST", url, headers=headers, data=payload)

    return response
```

* Now we need to update the payload sent to webex API with the *person* and the joke *text* we got as function argument.
For now the payload is a string, for simplicity let's keep it that way and let's add the variable *person* adn *text* at the correct position.
````python
    payload = "{\n    \"toPersonEmail\" : \""+person+"\",\n    \"text\" : \""+text+"\"\n}"
````

* The final file should look like this :
```python
import requests

def send_message_webex(person : str,text : str) -> requests.Response:
    url = "https://webexapis.com/v1/messages"

    payload = "{\n    \"toPersonEmail\" : \""+person+"\",\n    \"text\" : \""+text+"\"\n}"
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer MGRiYjIyOWUtOTFmZC00NDFkLTg2NWMtMDU0Y2JjNTBkZDYwYjdhYjdlNmEtNTNh_PF84_1eb65fdf-9643-417f-9974-ad72cae0e10f'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return response
```

### Task 8. Refactor code

* Let's test it out locally ! Add your CEC into the function first argument
```python
if __name__ == "__main__":
    print(send_message_webex("YOUR_CEC@cisco.com","This is clearly NOT a joke !").json())
```

Then just run the file from vsCode or from a terminal run :

Unix
```
python3 jokeBot/webexApi.py 
```

Windows
```
python jokeBot/webexApi.py 
```

```
➜  programability-day python3 jokeBot/webexApi.py 
{'id': 'Y2lzY29zcGFyazovL3VzL01FU1NBR0UvMTY2MmU2ODAtNjI1My0xMWViLTlmY2QtZDNjYzdhMmRjMjlk', 'roomId': 'Y2lzY29zcGFyazovL3VzL1JPT00vZTgyN2FlMmEtMWNhOC0zYmRhLWJiZmYtYjJhYTY0NDljMjI5', 'toPersonEmail': 'gladhuie@cisco.com', 'roomType': 'direct', 'text': 'This is clearly NOT a joke !', 'personId': 'Y2lzY29zcGFyazovL3VzL1BFT1BMRS81ZmYyMTdjYy0zZDgxLTQ4ZWItOWExZi1lNWNkMDI1ZjA1NTk', 'personEmail': 'joke@webex.bot', 'created': '2021-01-29T16:57:36.232Z'}
```

---

## Python REST API framework

* Ok we are getting there ! 
Now let's package your work into a simple python REST API uService. 
For that we will be leveraging a python Framework to prototype REST API : [FastAPI](https://fastapi.tiangolo.com)

* Building REST API using python is really simple but it's not the topic for today's workshop. For now you can open the *app.py* file and review its content.
__If__ the testing of previous function went smoothly, then you have nothing to fear !

### Task 9. Test your REST API locally

* If you'd like to test locally the API before packaging your application in a container you can do it running that command :

Unix
```
cd jokeBot
uvicorn app:rest --port 8080 --reload
```
```
uvicorn         : That's the ASGI server.    
app:rest        : We ask ASGI server to serve the application 'rest' from 'app.py' file.
--port 8080     : Server will run on localhost port 8080.
--reload        : Handy developement parameter, anytime you modify the app the application reload itself.
```

```
uvicorn app:rest --port 8080 --reload
INFO:     Uvicorn running on http://127.0.0.1:8080 (Press CTRL+C to quit)
INFO:     Started reloader process [96278] using statreload
INFO:     Started server process [96295]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
```

Windows
```
cd .\jokeBot\
python -m uvicorn app:rest --port 8080 --reload
```
* If you have any error reach a proctor to help you with your code then stop the code by issuing CRTL+C and restart.

Note: you need to be in jokeBot folder for the command to work properly, otherwise you will get the following error message:
```
ERROR:    Error loading ASGI app. Could not import module "app".
```

### Task 10. Open your swagger API 

* If you don't have any error open a web browser to [http://localhost:8080](http://localhost:8080)
* Click the `GET /joke` section 
* Click `Try it out` then `Execute`
![](./img/6.png)
* The result of your API should be in the Response section
![](./img/7.png)

  
#### Congratulations you made an API in python !

---

> :warning: *If you were not able to install Docker skip this section*
> 
## Ship it with Docker

* We now have to build a container with our code in it, with library and dependencies, and make it available as a web service
Open the Dockerfile and take time to review its content.

* When you are confident let's build the image !
```
docker build -t botjokes:latest .
```
```
➜  programability-makers git:(master) ✗ pwd
/Users/gladhuie/dev/programability-makers
➜  programability-makers git:(master) ✗ docker build -t botjokes:latest .
[...]
Successfully built a3929ee8d139
Successfully tagged botjokes:latest
```

* If everything goes well you should get an image available :
```
docker images
```

```
➜  programability-day docker images
REPOSITORY       TAG            IMAGE ID       CREATED         SIZE
botjokes         latest         a3929ee8d139   2 hours ago     55.4MB
```

* Let's try the container ! By issuing the following command :
```
docker run -it --rm --name botjokes -p 8080:80 botjokes:latest

-it         : Starts the container in interactive mode
--rm        : Delete the container when stopped
--name      : Give the container a name
-p 8080:80  : Do a PAT on your PC port 8080 to container port 80.
```

```
➜  programability-day docker run -it --rm --name botjokes -p 8080:80 botjokes:latest
INFO:     Started server process [1]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://0.0.0.0:80 (Press CTRL+C to quit)
```

* If you don't have any error open a web browser to [http://localhost:8080](http://localhost:8080)

* If you have any error reach a proctor to help you with your code then stop the container by issuing CRTL+C and restart from step 5) and rebuild you container.

* Try out your joke API !