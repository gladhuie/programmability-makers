#The container starts with a tiny base image with just python installed on it
FROM python:3.8-alpine

#If you are on Cisco VPN you might need to force an HTTP_PROXY for python pip to correctly install packages
#Just remove '#' from all folowing ENV line before building the container
#ENV http_proxy=http://proxy-wsa.esl.cisco.com:80/
#ENV HTTP_PROXY=http://proxy-wsa.esl.cisco.com:80/
#ENV https_proxy=http://proxy-wsa.esl.cisco.com:80/
#ENV HTTS_PROXY=http://proxy-wsa.esl.cisco.com:80/

#Copy the python requirements file into the container
COPY ./requirements.txt /requirements.txt

#Install all python package dependencies based on the requirements.txt
RUN pip install -r requirements.txt

#Copy the folder that contains our little bot logic to the container
COPY ./jokeBot /jokeBot

#Set the working directory into the container as /jokeBot
WORKDIR /jokeBot

#Set the command to be executed when you start the container
#Here we are asking an ASGI server to serve our app on port 80 of the container
CMD ["uvicorn", "app:rest", "--host", "0.0.0.0", "--port", "80"]