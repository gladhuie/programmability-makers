"""Here we are importing mandatory libraries and your precious code !"""
from fastapi import FastAPI
from starlette.responses import RedirectResponse,JSONResponse
from jokeApi import get_a_joke
from webexApi import send_message_webex

rest = FastAPI()

@rest.get('/')
async def root():
    """
    The root "/" of our small API. It just here to automatically redirect to our documentated API :)
    """
    response = RedirectResponse(url='/docs')
    return response

@rest.get("/joke")
async def joke():
    """
    This function return a joke.

    :return response: the HTTP return payload
    """

    return JSONResponse(get_a_joke())

@rest.post("/joke")
async def joke(person:str):
    """
    This function sends a joke to a person on webex teams.

    Take a look at the function decorator:
    @rest.post("/joke")

    It's an handy Python notation to explain that the following function should only be triggered on "POST" actions, for the URL "/joke"

    Note that you did not had to modify this function for the Bot to work.
    Although it was a simple exercise, you understand the importance on having specifications on what a function should have has arguments / should return.

    :param person: the person you are sending a joke to. Must be in the for <CEC>@cisco.com
    :return response: the HTTP return payload
    """

    joke = get_a_joke()

    response = send_message_webex(person,joke)

    return response.json()






