import requests

def send_message_webex(person : str,text : str) -> requests.Response:
    """ You must implement that function.

    The function takes 2 parameters :
        person : it's a string it will be the CEC the bot must send message to
        text : the text in string format that should be sent to the person

    The function must return the Response object you got from Webex Teams API
    """
    return



if __name__ == "__main__":
    """ 
    This part of the code will only be executed if you run directly webexApi.py file.
    It will not be visible to our app.py.
    
    It's often used to test individually a module.
    
    You should update the "YourCEC@cisco.com" variable with... your CEC :)
    """
    print(send_message_webex("YourCEC@cisco.com","This is clearly NOT a joke !").json())