import requests

def get_a_joke() -> str:
    """ You must implement that function.

    The function doesn't take argument

    The function must return a String, well.. the joke you fetched :)
    """
    return ""


if __name__ == "__main__":
    """ 
    This part of the code will only be executed if you run directly jokeAPI.py file.
    It will not be visible to our app.py.

    It's often used to test individually a module.
    """
    print(get_a_joke())